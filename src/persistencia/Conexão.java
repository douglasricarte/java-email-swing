/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.SQLException;
import java.sql.DriverManager;
/**
 *
 * @author dougl
 */
public class Conexão {
    private Connection conexão;

    public void fecharConecão(){
        try {
            conexão.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexão.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    public Connection abrirConexão(){
        String url = "jdbc:mysql://localhost:3306/projeto?useTimezone=true&serverTimezone=UTC";
        String user = "root";
        String password = "";
        
        try {
            conexão = DriverManager.getConnection(url, user, password);
        } catch (SQLException ex) {
            Logger.getLogger(Conexão.class.getName()).log(Level.SEVERE, null, ex);

        }
        
        return conexão;
    }
    
}
