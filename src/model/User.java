/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Random;

/**
 *
 * @author dougl
 */
public class User {
    private String nome;
    private String senha;
    private String email;

    
    
    public User(){
    
    }
    public String sortear(){
        Random gerador = new Random();
        int aux[];
        String tokem = "";
        for(int i = 0; i<6; i++){
            tokem = tokem+Integer.toString(gerador.nextInt(9));
        }
        return tokem;
    }
    
    
    public User(String nome, String senha, String email) {
        this.nome = nome;
        this.senha = senha;
        this.email = email;
        this.tokem = sortear();
    }
    private String tokem;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTokem() {
        return tokem;
    }

    public void setTokem(String tokem) {
        this.tokem = tokem;
    }
}
